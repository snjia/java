package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class JdbcUtilsTest2 {

    public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        try {
            conn = JdbcUtils.getConn();
            psmt = conn.prepareStatement("select * from member where id = ?");
            psmt.setInt(1, 1);
            rs = psmt.executeQuery();
            while (rs.next()){
                System.out.println(rs.getString("name"));
            }
        }catch (Exception e){
            throw new RuntimeException();
        }finally {
            JdbcUtils.close(conn, psmt, rs);
        }
    }

}
