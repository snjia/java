package jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class JdbcUtilsTest1 {

    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            conn = JdbcUtils.getConn();
            st = conn.createStatement();
            rs = st.executeQuery("select * from member");
            while (rs.next()){
                System.out.println(rs.getString("name"));
            }
        }catch (Exception e){
            throw new RuntimeException();
        }finally {
            JdbcUtils.close(conn, st, rs);
        }
    }

}
