package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCTest {


    public static void main(String[] args) throws Exception {
        //1.注册驱动
        Class.forName("com.mysql.jdbc.Driver");

        //2.获得连接
        String url = "jdbc:mysql://localhost:3306/test";
        Connection conn = DriverManager.getConnection(url, "root", "");

        //3.获得执行sql语句的对象
        Statement st = conn.createStatement();

        //4.执行sql语句
        //int executeQuery: 执行select语句
        //ResultSet executeUpdate: 执行insert, update, delete语句
        //boolean execute: 仅当select时并且有结果返回true, 其他返回false
        ResultSet rs = st.executeQuery("select * from member");

        //5.处理结果集
        while (rs.next()){
            //getObject(String name); 获取任意对象
            //getString(String name); 获取字符串
            //getInt(String name); 获取整型
            System.out.println(rs.getString("name"));
        }

        //6.释放资源, 先得到的先关闭
        rs.close();
        st.close();
        conn.close();
    }

}
