package com._54programer.factory.method.order;


import com._54programer.factory.method.pizza.LDCheesePizza;
import com._54programer.factory.method.pizza.LDPepperPizza;
import com._54programer.factory.method.pizza.Pizza;

public class LDOrderPizza extends OrderPizza {

	
	@Override
	Pizza createPizza(String orderType) {
	
		Pizza pizza = null;
		if(orderType.equals("cheese")) {
			pizza = new LDCheesePizza();
		} else if (orderType.equals("pepper")) {
			pizza = new LDPepperPizza();
		}
		// TODO Auto-generated method stub
		return pizza;
	}

}
