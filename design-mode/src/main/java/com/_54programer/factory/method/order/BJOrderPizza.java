package com._54programer.factory.method.order;


import com._54programer.factory.method.pizza.BJCheesePizza;
import com._54programer.factory.method.pizza.BJPepperPizza;
import com._54programer.factory.method.pizza.Pizza;

public class BJOrderPizza extends OrderPizza {

	
	@Override
	Pizza createPizza(String orderType) {
	
		Pizza pizza = null;
		if(orderType.equals("cheese")) {
			pizza = new BJCheesePizza();
		} else if (orderType.equals("pepper")) {
			pizza = new BJPepperPizza();
		}
		// TODO Auto-generated method stub
		return pizza;
	}

}
