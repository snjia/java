package com._54programer.factory.simple.factory;

import com._54programer.factory.simple.pizza.CheesePizza;
import com._54programer.factory.simple.pizza.GreekPizza;
import com._54programer.factory.simple.pizza.PepperPizza;
import com._54programer.factory.simple.pizza.Pizza;

public class SimpleFactory {

	public Pizza createPizza(String orderType) {

		Pizza pizza = null;

		if (orderType.equals("greek")) {
			pizza = new GreekPizza();
			pizza.setName(" GreekPizza ");
		} else if (orderType.equals("cheese")) {
			pizza = new CheesePizza();
			pizza.setName(" CheesePizza ");
		} else if (orderType.equals("pepper")) {
			pizza = new PepperPizza();
			pizza.setName("PepperPizza");
		}
		
		return pizza;
	}

}
