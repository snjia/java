package com._54programer.factory.abs.factory;

import com._54programer.factory.abs.pizza.*;

public class LdFactory implements AbsFactory {
    public Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if (orderType.equals("cheese")){
            pizza = new LdCheesePizza();
        }else if(orderType.equals("pepper")){
            pizza = new LdPepperPizza();
        }
        return pizza;
    }
}
