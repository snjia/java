package com._54programer.factory.abs.order;

import com._54programer.factory.abs.factory.AbsFactory;
import com._54programer.factory.abs.pizza.Pizza;

public class OrderPizza {

    private AbsFactory factory;

    public OrderPizza(AbsFactory factory) {
        this.factory = factory;
    }

    public void orderPizza(String orderType){
        Pizza pizza = this.factory.createPizza(orderType);
        if (pizza != null){
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
            System.out.println("订购成功");
        }else {
            System.out.println("订购失败");
        }
    }
}
