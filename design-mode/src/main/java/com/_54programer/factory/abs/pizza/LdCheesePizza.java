package com._54programer.factory.abs.pizza;

public class LdCheesePizza extends Pizza {
    public void prepare() {
        setName("伦敦奶酪披萨");
        System.out.println("伦敦奶酪披萨准备原材料 ");
    }
}
