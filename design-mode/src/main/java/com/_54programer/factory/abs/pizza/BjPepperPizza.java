package com._54programer.factory.abs.pizza;

public class BjPepperPizza extends Pizza {
    public void prepare() {
        setName("北京胡椒披萨");
        System.out.println("北京胡椒披萨准备原材料 ");
    }
}
