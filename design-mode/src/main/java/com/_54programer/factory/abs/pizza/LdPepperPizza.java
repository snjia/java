package com._54programer.factory.abs.pizza;

public class LdPepperPizza extends Pizza {
    public void prepare() {
        setName("伦敦胡椒披萨");
        System.out.println("伦敦胡椒披萨准备原材料 ");
    }
}
