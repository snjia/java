package com._54programer.factory.abs.factory;

import com._54programer.factory.abs.pizza.Pizza;

/**
 * 抽象接口
 */
public interface AbsFactory {

    /**
     * 根据订单类型生产对应的Pizza
     * @param orderType
     * @return
     */
    Pizza createPizza(String orderType);

}
