package com._54programer.factory.abs.pizza;

public class BjCheesePizza extends Pizza {
    public void prepare() {
        setName("北京奶酪披萨");
        System.out.println("北京奶酪披萨准备原材料 ");
    }
}
