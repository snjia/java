package com._54programer.factory.abs;

import com._54programer.factory.abs.factory.LdFactory;
import com._54programer.factory.abs.order.OrderPizza;

public class Test {

    public static void main(String[] args) {
        OrderPizza orderPizza = new OrderPizza(new LdFactory());
        orderPizza.orderPizza("cheese");
    }

}
