package com._54programer.factory.abs.factory;

import com._54programer.factory.abs.pizza.BjCheesePizza;
import com._54programer.factory.abs.pizza.BjPepperPizza;
import com._54programer.factory.abs.pizza.Pizza;

public class BjFactory implements AbsFactory {
    public Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if (orderType.equals("cheese")){
            pizza = new BjCheesePizza();
        }else if(orderType.equals("pepper")){
            pizza = new BjPepperPizza();
        }
        return pizza;
    }
}
