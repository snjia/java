package com._54programer.singleton.ehan;

/**
 * 饿汉式: 静态变量
 * 优点: 类装载的时候实例化, 避免线程同步问题
 * 缺点: 如果一次不用, 造成内存浪费
 */
public class Singleton1 {

    //构造器私有
    private Singleton1(){}

    private final static Singleton1 instance = new Singleton1();

    public static Singleton1 getInstance(){
        return instance;
    }

}