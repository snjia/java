package com._54programer.singleton.ehan;

/**
 * 饿汉式: 静态代码块
 * 优点: 类装载的时候实例化, 避免线程同步问题
 * 缺点: 如果一次不用, 造成内存浪费
 */
public class Singleton2{

    //构造器私有
    private Singleton2(){}

    //静态代码块
    private static Singleton2 instance;
    static {
        instance = new Singleton2();
    }

    public static Singleton2 getInstance(){
        return instance;
    }

}


