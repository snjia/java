package com._54programer.singleton.lhan;

/**
 * 枚举: 借助JDK1.5提供的枚举来实现单例模式, 不仅能避免多线程同步的问题, 而且还能防止反序列化重新创建新的对象
 *
 * 推荐使用
 *
 * 这种方式是Effective Java作者Josh Bloch提倡的方式
 *
 */
public enum Singleton6 {
    INSTANCE;
    public void say(){
        System.out.println("say....");
    }
}
