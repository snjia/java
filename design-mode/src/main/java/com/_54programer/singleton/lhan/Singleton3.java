package com._54programer.singleton.lhan;

/**
 * 饿汉式: 同步代码块, 线程安全, 不推荐使用
 * 效率太低
 */
public class Singleton3 {

    private static Singleton3 instance;

    private Singleton3(){}

    public static Singleton3 getInstance(){
        if (instance == null){
            synchronized (Singleton3.class){
                instance = new Singleton3();
            }
        }
        return instance;
    }

}
