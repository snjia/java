package com._54programer.singleton.lhan;

/**
 * 饿汉式: 静态内部类, 推荐使用
 *
 * 1.这种方式采用了类装载的机制来保证初始化实例时只有一个线程
 * 2.静态内部类的Singleton5不会在类被装载时实例化, 而是在需要时调用getInstance方法才会装载Singlton5Instance类
 * 3.类的静态属性只会在第一次加载的时候初始化, 这里, JVM帮我们保证了线程的安全性, 在类进行初始化时, 别的线程是无法进入的
 */
public class Singleton5 {

    private static volatile Singleton5 instance;

    private Singleton5(){}

    /**
     * 静态内部类, 该类中又一个静态属性
     */
    private static class Singlton5Instance{
        private static final Singleton5 INSTANCE = new Singleton5();
    }

    public static synchronized Singleton5 getInstance(){
        return Singlton5Instance.INSTANCE;
    }

}
