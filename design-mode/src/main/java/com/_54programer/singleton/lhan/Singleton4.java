package com._54programer.singleton.lhan;

/**
 * 饿汉式: 双重检查, 延迟加载, 线程安全, 推荐使用。
 *
 * double-check是多线程开发中经常使用的
 *
 */
public class Singleton4 {

    private static volatile Singleton4 instance;

    private Singleton4(){}

    public static Singleton4 getInstance(){
        if (instance == null){
            synchronized (Singleton4.class){
                if (instance == null){
                    instance = new Singleton4();
                }
            }
        }
        return instance;
    }

}
