package com._54programer.singleton.lhan;

/**
 * 饿汉式: 线程不安全
 * 多线程下, 可能会产生多个实例。不推荐使用。
 */
public class Singleton1 {

    private static Singleton1 instance;

    private Singleton1(){}

    public static Singleton1 getInstance(){
        if (instance == null){
            instance = new Singleton1();
        }
        return instance;
    }

}
