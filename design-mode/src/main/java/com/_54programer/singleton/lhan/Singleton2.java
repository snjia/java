package com._54programer.singleton.lhan;

/**
 * 饿汉式: 同步方法, 线程安全, 不推荐使用
 * 效率太低
 */
public class Singleton2 {

    private static Singleton2 instance;

    private Singleton2(){}

    public static synchronized Singleton2 getInstance(){
        if (instance == null){
            instance = new Singleton2();
        }
        return instance;
    }

}
