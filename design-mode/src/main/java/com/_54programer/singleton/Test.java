package com._54programer.singleton;

import com._54programer.singleton.ehan.Singleton1;
import com._54programer.singleton.ehan.Singleton2;
import com._54programer.singleton.lhan.Singleton6;

public class Test {


    public static void main(String[] args) {
        test6();
    }

    private static void test1() {
        Singleton1 instance1 = Singleton1.getInstance();
        Singleton1 instance2 = Singleton1.getInstance();
        System.out.println(instance1);
        System.out.println(instance2);
    }

    private static void test2() {
        Singleton2 instance1 = Singleton2.getInstance();
        Singleton2 instance2 = Singleton2.getInstance();
        System.out.println(instance1);
        System.out.println(instance2);
    }

    private static void test6() {
        Singleton6 instance1 = Singleton6.INSTANCE;
        Singleton6 instance2 = Singleton6.INSTANCE;
        System.out.println(instance1.hashCode());
        System.out.println(instance2.hashCode());
        instance1.say();
        instance2.say();
    }
}
