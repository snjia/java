package com._54programer.observer;

import com._54programer.observer.observer.iml.Baidu;
import com._54programer.observer.observer.iml.Sina;

public class Test {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        Baidu baidu = new Baidu();
        weatherData.registerObserver(baidu);

        Sina sina = new Sina();
        weatherData.registerObserver(sina);

        weatherData.removeObserver(baidu);

        weatherData.setData(100f, 200f, 300f);
    }
}
