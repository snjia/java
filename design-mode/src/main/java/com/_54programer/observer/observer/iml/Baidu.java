package com._54programer.observer.observer.iml;

import com._54programer.observer.observer.Observer;

public class Baidu implements Observer {

    private float temperature;
    private float pressure;
    private float humidity;

    public void update(float temperature, float pressure, float humidity) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        display();
    }

    public void display(){
        System.out.println("baidu: "+this.temperature+", "+this.pressure+", "+this.humidity);
    }
}
