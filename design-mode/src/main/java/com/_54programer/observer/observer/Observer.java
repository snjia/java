package com._54programer.observer.observer;

/**
 * 观察者接口, 观察者来实现
 */
public interface Observer {

    /**
     * 更新天气
     * @param temperature
     * @param pressure
     * @param humidity
     */
    void update(float temperature, float pressure, float humidity);
}
