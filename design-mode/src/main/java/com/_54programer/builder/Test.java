package com._54programer.builder;

public class Test {
    public static void main(String[] args) {


        HouseDirector houseDirector = new HouseDirector(new CommonBuilder());
        House house = houseDirector.createHouse();
        System.out.println(house.toString());

        HouseDirector houseDirector1 = new HouseDirector(new HighBuilder());
        House house1 = houseDirector1.createHouse();
        System.out.println(house1.toString());


    }
}
