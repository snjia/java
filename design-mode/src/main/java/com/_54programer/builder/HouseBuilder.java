package com._54programer.builder;

/**
 * 抽象的建造者
 */
public abstract class HouseBuilder {

    protected House house = new House();

    //建造流程, 具体子类实现
    protected abstract void buildBasic();
    protected abstract void buildWall();
    protected abstract void buildRoofed();

    //获取建造好的房子
    public House buildHouse(){
        buildBasic();
        buildWall();
        buildRoofed();
        return house;
    }

}
