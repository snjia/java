package com._54programer.builder;

public class HighBuilder extends HouseBuilder{
    public void buildBasic() {
        super.house.setBaise("hign basic");
    }

    public void buildWall() {
        super.house.setWall("hign wall");
    }

    public void buildRoofed() {
        super.house.setRoofed("hign roofed");
    }
}
