package com._54programer.builder;

public class HouseDirector {


    private HouseBuilder houseBuilder;


    public HouseDirector(HouseBuilder houseBuilder) {
        this.houseBuilder = houseBuilder;
    }


    public House createHouse(){
        return houseBuilder.buildHouse();
    }
}
