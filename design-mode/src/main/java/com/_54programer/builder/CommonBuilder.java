package com._54programer.builder;

public class CommonBuilder extends HouseBuilder {
    public void buildBasic() {
        super.house.setBaise("common basic");
    }

    public void buildWall() {
        super.house.setWall("common wall");
    }

    public void buildRoofed() {
        super.house.setRoofed("common roofed");
    }
}
